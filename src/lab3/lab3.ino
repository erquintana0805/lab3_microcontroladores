/*
  Analog input, analog output, serial output
 
 Reads an analog input pin, maps the result to a range from 0 to 255
 and uses the result to set the pulsewidth modulation (PWM) of an output pin.
 Also prints the results to the serial monitor.
 
 The circuit:
 * potentiometer connected to analog pin 0.
 Center pin of the potentiometer goes to the analog pin.
 side pins of the potentiometer go to +5V and ground
 * LED connected from digital pin 9 to ground
 
 created 29 Dec. 2008
 modified 9 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.

 https://www.sparkfun.com/datasheets/LCD/Monochrome/Nokia5110.pdf



 */
#include <PID_v1.h>
#include <PCD8544.h>

const int temp_analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int hum_analogInPin = A1;  // Analog input pin that the potentiometer is attached to
const int heater_analogOutPin = 9; // Analog output pin that the LED is attached to

int temp_value = 0;        // value read from the pot
int hum_value = 0;
int heater_value = 0;        // value output to the PWM (analog out)

static const int ref_temp_min = 30;
static const int ref_temp_max = 32;
static const int ref_temp = 31;
static const int BLED = 13;
static const int RLED = 12;
static PCD8544 lcd;

void setup() {
    lcd.begin(84, 48);
    Serial.begin(9600); 
    pinMode(BLED, OUTPUT);
    pinMode(RLED, OUTPUT);

    Serial.print("temp," );          
    Serial.print("heater,");     
    Serial.println("hum");  

}

void loop() {
    // read the analog in value:
    temp_value = analogRead(temp_analogInPin);            
    // map it to the range of the analog out:
    temp_value = map(temp_value, 0, 1024, 28 , 42 );     

    // read the analog in value:
    hum_value = analogRead(hum_analogInPin);            
    // map it to the range of the analog out:
    hum_value = map(hum_value, 0, 1024, 0, 100);  

    if(temp_value < 30){
        digitalWrite(BLED, HIGH);
        heater_value = 0;
    } else if(temp_value > 32){
        digitalWrite(RLED, HIGH);
        heater_value = 1;       
    } else {
        digitalWrite(BLED, LOW);
        digitalWrite(RLED, LOW);
    }
    heater_value = map(temp_value, 28, 42, 0 , 253);  
    analogWrite(heater_analogOutPin, heater_value); 
    // print the results to the serial monitor:     
    Serial.print(temp_value);          
    Serial.print("," );          
    Serial.print(heater_analogOutPin);   
    Serial.print("," );  
    Serial.println(hum_value); 


    //LCD
    lcd.setCursor(0, 0);
    lcd.print("Temp: ");
    lcd.setCursor(10, 1);
    lcd.print(temp_value, DEC);
    delay(100);

    lcd.setCursor(0, 4);
    lcd.print("Hum: ");
    lcd.setCursor(10, 5);
    lcd.print(hum_value, DEC);
    delay(100);

    lcd.setCursor(0, 2);
    lcd.print("Ref temp: ");
    lcd.setCursor(10, 3);
    lcd.print(ref_temp, DEC);
    delay(100);


    delay(100);                     
}

